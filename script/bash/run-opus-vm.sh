#!/bin/bash
clear
file=$OPUS/ssh.properties
declare -a tab_choice
ssh_address=""

echo "
         ▄██████▄     ▄████████▄   ██▄    ███    ▄████████
        ███    ███   ███     ███   ███    ███   ███    ███
        ███    ███   ███     ███   ███    ███   ███    █▀
        ███    ███   ███     ███   ███    ███   ███
        ███    ███  ▀█████████▀    ███    ███   ▀█████████
        ███    ███   ███           ███    ███          ███
        ███    ███   ███           ███    ███    ▄█    ███
        ▀████████▀ ▄████▀          ▀████████▀  ▄████████▀

        𝒃𝒚 𝒏𝒂𝒃𝒊𝒍𝒆
"

# FUNCTION

# get value from key
get_properties_by_choice () {
  if [ -f "$file" ]
  then
    while IFS='=' read -r key value
    do
      if [ "$1" == "$key" ]
      then
        ssh_address=$value
      fi
    done < "$file"
  else
    echo "$file not found."
  fi
}

# init the table $tab_choice with all key name from ur properties files
set_tab_choice_by_properties_key() {
  if [ -f "$file" ]
    then
      i=0
      while IFS='=' read -r key value
      do
        tab_choice[i]=$key
        ((i=i+1))
      done < "$file"
      tab_choice[((i+1))]="exit"
    else
      echo "$file not found."
    fi
}

run_ssh () {
  ssh $1
}

nav_choice () {
  PS3='Please enter your choice: '
  #auto completion
  select opt in "${tab_choice[@]}"
  do
    for i in "${tab_choice[@]}"
    do
      if [ "$opt" == "$i" ]
      then
          if [ "$opt" == "exit" ]
          then
            clear
            exit
          fi
          clear
          get_properties_by_choice "$opt"
          run_ssh "$ssh_address"
          exit
        fi
    done
  done
}

# EXEC
set_tab_choice_by_properties_key
nav_choice